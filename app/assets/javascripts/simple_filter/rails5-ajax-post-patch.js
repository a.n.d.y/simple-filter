/** Rails dropped jQuery as default script language!
 * Now Ajax.Post-Request are blocked to prevent CrossSiteScripting attacks.
 * This patch adds the necessary token to the http-header of all Ajax-Calls.
 */

jQuery(function($) {

    $(function () {
        $.ajaxSetup({
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
        });
    });

});