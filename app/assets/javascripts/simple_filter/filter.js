/** filter **/
jQuery(function($){
	
    var postFilter = function(formElement,trigger){
        $.ajax({
            url: "/simple_filter/filter/set_filter",
            type: "post",
            dataType: "html",
            data: $(formElement).serialize(),
            success: function(returnData){
                var filterRemote = $(formElement).data("filter-remote")
                var params = {simple_filter: {filter_id: $(formElement).find('#filter_id').val()}};
                if(filterRemote == true){
                    var targetPath = window.location.pathname;
                    $.ajax({
                        url: targetPath,
                        type: "GET",
                        dataType: "script",
                        data: params
                    });
                } else {
                    window.location.reload();
                }
            },
            error: function(e){
                alert(e);
            }
        });
    }

    $('.simple-filter-container a.set-filter').on('click',function(event){
        event.preventDefault();
        var form = $('#'+$(this).data('target-filter-form'));
        postFilter(form,$(this));
    });

    $('.simple-filter-container a.reset-filter').on('click',function(event){
        event.preventDefault();
        var form = document.getElementById($(this).data('target-filter-form'));

        /** easier to work with native javascript here **/
        for(var i = 0; i < form.elements.length; i++){
            if(form.elements[i].type == 'text' || form.elements[i].type == 'number'){
                form.elements[i].value = '';
            }else if(form.elements[i].type == 'select-one'){
                form.elements[i].value = '';
                var is_conditon_select = false;
                for(var xxx=0;xxx<form.elements[i].options.length;xxx++){
                    if(form.elements[i].options[xxx].value == 'not_active') is_condition_select = true;
                }
                if(is_condition_select == true) {
		    form.elements[i].value = 'not_active';
		    /** trigger 'change' event to execute the onChange-listeners **/
		    var event = new Event('change');
		    form.elements[i].dispatchEvent(event);
		}
            }else if(form.elements[i].type == 'select-multiple'){
                form.elements[i].value = [];
            }else if(form.elements[i].type == 'checkbox'){
                form.elements[i].checked = false;
            }
        };

        postFilter(form,$(this));
    });

    $.fn.disableFirstInput = function(){
        var input = $('#'+$(this).attr('id').replace('condition','value_1'));		
	  if($.inArray($(this).val(),['not_active']) >-1){
              /** disable input if filter is not active**/
	      if(input.hasClass('simple-filter-checkbox-container')){
	        input.find('input[type=checkbox]').each(function(){ 
	          $(this).prop("checked",false) 
		  $(this).attr("disabled","disabled") 
		});     
	      }else{
                input.val('');
                input.attr('disabled','disabled');
	      }
          }else{
	    if(input.hasClass('simple-filter-checkbox-container')){
	      input.find('input[type=checkbox]').each(function(){ 
	        $(this).removeAttr('disabled');
	      }); 
	    }else{
              input.removeAttr('disabled');
	    }
          }
    };
    
    $.fn.disableSecondInput = function(){
        var input = $('#'+$(this).attr('id').replace('condition','value_2'));
        if($.inArray($(this).val(),['is','is_not','smaller_than','bigger_than', 'not_active']) >-1){
            /** disable second field for range filtering**/
            input.val('');
            input.attr('disabled','disabled');
        }else{
            input.removeAttr('disabled');
        }
    };

    
    $('.simple-filter-container .simple-filter-condition-select').on('change', function(){
        $(this).disableFirstInput();
	if($(this).hasClass('integer-condition') || $(this).hasClass('decimal-condition') || $(this).hasClass('date-condition') || $(this).hasClass('datetime-condition')){
	  $(this).disableSecondInput();
	}
    });


    
    
    
    
    
    
    /**** EVENTS ON PAGE-LOAD ****/
    $(document).ready(function() {
	
        $('.simple-filter-container .simple-filter-condition-select').each(function(){
            $(this).disableFirstInput();
	    if($(this).hasClass('integer-condition') || $(this).hasClass('decimal-condition') || $(this).hasClass('date-condition') || $(this).hasClass('datetime-condition')){
	      $(this).disableSecondInput();
	    }
        });

        //init bootstrap-datepickers
        $('.bootstrap-datepicker').each(function(){
           $(this).datepicker();
        });
    });

});
