module SimpleFilter

  class FilterController < ApplicationController
  
    include SimpleFilter::FilterStorage


    #POST /simple_filter/filter/set_filter
    def set_filter

      filter_id = params[:filter_id].to_sym
      ar_model = params[:ar_model].to_s.underscore.to_sym

      filter_config = SimpleFilter.find_by_model_and_filter_name(ar_model,filter_id)

      filter_state = {}
      params[:filter].each do |filter_field, filter_params|

        filter_column_config = filter_config[filter_field.to_sym]

        condition = filter_params["condition"].to_s.to_sym
        value_1 = filter_params["value_1"]
        value_2 = filter_params["value_2"]

        filter_state[filter_field.to_sym] = {
            :condition => condition,
            :value_1 => value_1,
            :value_2 => value_2
        }
      end

      set_filter_state(filter_id, ar_model, filter_state)

      head :ok
    end

  
  end

end