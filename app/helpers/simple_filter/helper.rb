module SimpleFilter
  module Helper

    include ::SimpleFilter::FilterStorage
    include ::SimpleFilter::InputTagHelper

    def get_filter_label(filter_id, ar_model, config)
      if config[1][:label].present?
        return config[1][:label]
      elsif [:has_many_reflection, :belongs_to_reflection].include?(config[1][:filter_type])
        ar_reflection = ar_model.reflections.symbolize_keys[config[1][:reflection_name]]
	      return ar_reflection.klass.model_name.human
      else
	      return ar_model.to_s.classify.constantize.human_attribute_name(config[1][:column_name])
      end	
    end	    


    def render_simple_filter(filter_id, ar_model,options={})
      filter_id = filter_id.to_sym
      filter_config = ::SimpleFilter.find_by_model_and_filter_name(ar_model,filter_id)
      filter_states = get_filter_state(filter_id, ar_model)
      active_filter_count = filter_states.map{|key,value| true if value[:condition].to_sym != SimpleFilter::Condition::INACTIVE_CONDITION}.compact.count rescue 0
      filter_element_id = "simple-filter-form-#{ar_model.to_s.underscore}-#{filter_id}"

      #### panel options ####
      if options[:panel_header_title] == false
        options[:panel_collapsed] = false #no header => no collapse button	      
      else
        options[:panel_header_title] ||= ar_model.model_name.human(:count => 2)
        options[:panel_collapsed] ||= false
      end

      options[:panel_class] ||= 'panel-default'
      options[:filter_remote] ||= false
      ####
      
      
      content_tag(:div, class: ['panel', options[:panel_class], 'simple-filter-container']) do
	
	
        #### HEADER
        if options[:panel_header_title]
          content_tag(:div, class: ['panel-heading']) do
            concat(
              content_tag(:h4) do
                concat(options[:panel_header_title])
                concat(content_tag(:div, class: ['btn-group','pull-right']) do
                  content_tag(:button, type: :button, data: {toggle: "collapse", target: "##{filter_element_id}-content"}) do
                    concat(content_tag(:span, nil, class: ["glyphicon", "glyphicon-minus"]))
                  end
                end)
              end
            )
          end
        else
          ""
        end.html_safe + \



        ####collapsable container for panel-body and panel-footer
        content_tag(:div, id: "#{filter_element_id}-content" ,class: ["panel-collapse", options[:panel_collapsed] ? "collapse": ""]) do
        
	
	        #### BODY
	        content_tag(:div, class: ['panel-body']) do

            concat(content_tag(:form, class: ['form-horizontal'], id: filter_element_id, data: {"filter-remote" => options[:filter_remote]}) do
              concat(hidden_field_tag(:filter_id, filter_id))
              concat(hidden_field_tag(:ar_model, ar_model.to_s.underscore))


              filter_config.each do |config|

                filter_state = filter_states[config[0]] || {}
		
		            target_model = ar_model
		            if config[1][:reference].present?
                  reference_object = ar_model.reflections.symbolize_keys[config[1][:reference]]
                  target_model = reference_object.klass	  
                end

                concat(content_tag(:div, class: ['row']) do


                  concat(content_tag(:div, class: ['form-group-sm', 'col-sm-5', 'col-xs-12']) do
                    concat label_tag("filter[#{config[0].to_s}][condition]", get_filter_label(filter_id, target_model, config), class:['control-label','col-sm-5', 'col-xs-12'])
                    concat(content_tag(:div, class: ['col-sm-7', "col-xs-12"]) do
                      condition_select(filter_id, target_model, config, filter_state)
                    end)
                  end)


                  concat(content_tag(:div, class: ['form-group-sm', 'col-sm-7', 'col-xs-12']) do
                    case config[1][:filter_type]
                      when *[:string,:text]
                        concat(content_tag(:div, class: ["col-sm-6", "col-xs-12"]) do
                          text_field_tag("filter[#{config[0].to_s}][value_1]", filter_state[:value_1], class:["form-control"])
                        end)
                      when *[:integer,:decimal]
                        concat(content_tag(:div, class: ["col-xs-6"]) do
                          number_field_tag("filter[#{config[0].to_s}][value_1]", filter_state[:value_1], class:["form-control"])
                        end)
                        concat(content_tag(:div, class: ["col-xs-6"]) do
                          number_field_tag("filter[#{config[0].to_s}][value_2]", filter_state[:value_2], class:["form-control"])
                        end)
                      when *[:date, :datetime]
                        concat(content_tag(:div, class: ["col-xs-6"]) do
                          text_field_tag("filter[#{config[0].to_s}][value_1]", filter_state[:value_1], class:["form-control", "bootstrap-datepicker"], data:{"date-language" => I18n.locale, "date-format" => "dd-mm-yyyy", "date-autoclose" => true, "date-today-btn" => "linked", "date-days-of-week-highlighted" => [6,0]})
                        end)
                        concat(content_tag(:div, class: ["col-xs-6"]) do
                          text_field_tag("filter[#{config[0].to_s}][value_2]", filter_state[:value_2], class:["form-control", "bootstrap-datepicker"], data:{"date-language" => I18n.locale, "date-format" => "dd-mm-yyyy", "date-autoclose" => true, "date-today-btn" => "linked", "date-days-of-week-highlighted" => [6,0]})
                        end)
                      when :boolean
                        concat(content_tag(:div, class: ["col-sm-6", "col-xs-12"]) do
                          boolean_select(filter_id,target_model, config, filter_state)
                        end)
                      when :selection
                        concat(content_tag(:div, class: ["col-sm-6", "col-xs-12"]) do
                          selection_select(filter_id,target_model, config, filter_state)
                        end)
                      when *[:belongs_to_reflection, :has_many_reflection]
                        concat(content_tag(:div, class: ["col-sm-6", "col-xs-12"]) do
                          reflection_select(filter_id,target_model, config, filter_state)
                        end)
                    end
                  end)


                end)
              end

            end)

          end.html_safe + \
	
	
	
	        #### FOOTER
	        content_tag(:div, class: ['panel-footer']) do
	          concat(content_tag(:div, class: ['row']) do
              concat(content_tag(:div, class: ['col-xs-6', 'text-right']) do
                link_to I18n.t('simple_filter.buttons.set_filter'),'#',class: ['btn', 'btn-warning','set-filter'], data:{"target-filter-form" => filter_element_id}
              end)
              concat(content_tag(:div, class: ['col-xs-6', 'text-left']) do
                link_to I18n.t('simple_filter.buttons.reset_filter'),'#', class: ['btn', 'btn-warning','reset-filter'], data:{"target-filter-form" => filter_element_id}
              end)
            end)
	        end.html_safe
      

        end #END panel-collapse
      end #END simple-filter-container

    end


  end
end