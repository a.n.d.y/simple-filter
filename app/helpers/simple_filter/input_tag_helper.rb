module SimpleFilter
  module InputTagHelper

    include ::SimpleFilter::Condition

    def condition_select(filter_id,ar_model,config, filter_state)
      select_options = [[I18n.t("simple_filter.conditions.#{::SimpleFilter::Condition::INACTIVE_CONDITION}"), ::SimpleFilter::Condition::INACTIVE_CONDITION]]
      if config[1][:multiple] && config[1][:filter_type] == :has_many_reflection
        condition_method = "#{config[1][:filter_type]}_multiple_conditions"
      else
        condition_method = "#{config[1][:filter_type]}_conditions"
      end
      ::SimpleFilter::Condition.send(condition_method).each do |condition|
        select_options << [I18n.t("simple_filter.conditions.#{condition}"), condition]
      end
      selected = filter_state[:condition].to_sym rescue ::SimpleFilter::Condition::INACTIVE_CONDITION
      select_tag("filter[#{config[0].to_s}][condition]", options_for_select(select_options,selected), class: ["form-control", "simple-filter-condition-select","#{config[1][:filter_type]}-condition"])
    end


    def single_select(filter_id, ar_model, config, select_options, filter_state)
      selected = filter_state[:value_1]
      select_tag("filter[#{config[0].to_s}][value_1]", options_for_select(select_options, selected), include_blank: true, class: ["form-control"])
    end


    def multi_select(filter_id, ar_model, config, select_options, filter_state)
      selected = filter_state[:value_1]

      if config[1][:multiple] == :checkbox
      	content_tag(:div, id: "filter_#{config[0].to_s}_value_1", class: ['simple-filter-checkbox-container']) do
          select_options.each do |option|
            concat(content_tag(:div, class: ['checkbox', 'well well-sm']) do
              concat(label_tag("filter[#{config[0].to_s}][value_1][#{option[1]}]") do
                concat(check_box_tag("filter[#{config[0].to_s}][value_1][]", option[1],(selected.is_a?(Array) && selected.map(&:to_s).include?(option[1].to_s)),{class:[], id: "filter_#{config[0].to_s}_value_1_#{option[1]}"}))
                concat(option[0])
              end)
            end)
          end
	      end
      else
        select_tag("filter[#{config[0].to_s}][value_1]", options_for_select(select_options, selected), include_blank: true,class: ["form-control"], :multiple => true, :size => 3)
      end

    end


    def boolean_select(filter_id, ar_model, config, filter_state)
      select_options = [
        [I18n.t("simple_filter.boolean_select.true"), 1], 
	      [I18n.t("simple_filter.boolean_select.false"), 0]
      ]
      single_select(filter_id, ar_model, config, select_options, filter_state)
    end


    def selection_select(filter_id, ar_model, config, filter_state)
      select_options = []
      config[1][:select_options].each do |option|
        id = option[0]
        value = option[1]
        select_options << [value, id]
      end

      if config[1][:multiple]
        multi_select(filter_id, ar_model, config, select_options, filter_state)
      else
        single_select(filter_id, ar_model, config, select_options, filter_state)
      end
    end


    def reflection_select(filter_id, ar_model, config, filter_state)
      select_options = []

      ar_reflection = ar_model.reflections.symbolize_keys[config[1][:reflection_name]]
      if [:belongs_to, :has_many].include? ar_reflection.macro
        klass = ar_reflection.klass

        (config[1][:scopes] ||= []).each do |_scope|
          klass = klass.send(_scope)
        end

        klass.all.each do |record|
          id = record.send(ar_reflection.association_primary_key)
          value_method = config[1][:value_method]
          if value_method.respond_to?(:call)
            value = value_method.call(record)
          else
            value = record.send(value_method)
          end
          select_options << [value,id]
        end

      else
        raise ::SimpleFilter::UnhandledReflectionType, ar_reflection.macro
      end

      if config[1][:multiple]
        multi_select(filter_id, ar_model, config, select_options, filter_state)
      else
        single_select(filter_id, ar_model, config, select_options, filter_state)
      end
    end
  
  
  
  
  end
end