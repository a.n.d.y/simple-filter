Rails.application.routes.draw do
  namespace :simple_filter do
    resource :filter, controller: :filter, only: [] do
      post :set_filter
    end
  end
end
