# SimpleFilter
SimpleFilter is a small RailsEngine which offers an easy filter-setup with only a few lines of code.

It includes a rudimentary **Bootstrap-UI** and a **QueryBuilder** that fit the needs for the most common filter requirements. 

## Features
  * Supports filtering on common ActiveRecord data types **(:string, :text, :integer, :decimal, :date, :boolean)**
  * Supports filtering on **:belongs_to**- and **:has_many**-reference models
  * Provides a various set of filter conditions depending on the datatype
  * Out-of-the-box **Filter-UI** with Bootstrap3 Grid-Layout
  * Includes [Bootstrap Datepicker](https://bootstrap-datepicker.readthedocs.io/en/latest/index.html) for :date/:datetime-filters
  * Provides **Range-Filtering** on :date-, :datetime-, :integer- and :decimal-data types
  * Provides filtering for **multiple string values** if you pass **";"** as string separator
  * Supports filtering via **Ajax**
  * State of filters is stored in the session. So filters don't get lost between page loads.
  * A few configuration options to fit SimpleFilter to your needs

## Dependencies
  * SimpleFilter is tested in a Rails 5.1 (MySQL) environment. No guarantee for other Rails versions or database engines!
  * jQuery
  * Bootstrap3

## Included libs
  * [Bootstrap Datepicker](https://bootstrap-datepicker.readthedocs.io/en/latest/index.html)

## Live Demo
Checkout the [live demo](http://filter-demo.stark-development.de) and see **SimpleFilter** in action.


## Installation

Add this line to your application's Gemfile:

```ruby
gem 'simple_filter', :git => 'https://gitlab.com/a.n.d.y/simple-filter'
```

And then execute:

    $ bundle install

Add necessary assets to your Rails app:
```ruby
#app/assets/javascripts/application.js
//= require simple_filter
```
```ruby
#app/assets/stylesheets/application.css
*= require simple_filter
```

## Usage
We setup an example filter for a user model. 

We want filter on the **:firstname**- and **:lastname**-columns and on the **belongs_to :country**-references
#### 1. Configure your `Model`
`setup_filter_for(filter_id, column_or_reference, options)`
```ruby
class User < ApplicationRecord
  
  belongs_to :country
  
  setup_filter_for :my_first_filter, :firstname
  setup_filter_for :my_first_filter, :lastname
  
  setup_filter_for :my_first_filter, :country, value_method: :name
end
```
#### 2. Configure your `Controller`
`build_filtered_relation(filter_id, model)`
```ruby
class UsersController < ApplicationController
  # GET /users
  def index
    @users = build_filtered_relation(:my_first_filter, User)
  end
end
```
#### 3. Render the SimpleFilter in your `View`
`render_simple_filter(filter_id, model, options)`
```erb
<!-- /app/views/users/index.html.erb -->

<%= render_simple_filter(:my_first_filter,User) %>

<!-- below could be your users list -->
<% @users.each do |user| %>
  <!-- do user stuff -->
<% end %>
```
### That's all! Your first filter is ready to use!






## Configurations
#### Selections
Selection filter on **reference tables** needs the `:value_method` option to tell SimpleFilter which attribute should be displayed in the select-options.

**:value_method** accepts also a **Proc-Object**.

If you want to influence the list of reference items, you can pass usual **model scopes** in your configuration with the `:scopes` option.

**:scopes** have to be an **Array**.
```ruby
class User < ApplicationRecord
  belongs_to :country
  
  setup_filter_for :my_first_filter, :country, scopes: [:sorted, :not_deleted], value_method: Proc.new {|record| record.name + " (#{record.abbreviation})"} 
end
```
If you have a **custom collection** without a reference table you have to use the `:select_options` option.

Important is the order with `[[key,display-value], [key,display-value], ...]`. Keys can also be of type **:string**.
```ruby
class User < ApplicationRecord
  GENDERS = [
    [1, "Female"].freeze,
    [2, "Male"].freeze
  ].freeze
  
  setup_filter_for :my_first_filter, :gender_id, select_options: GENDERS  
end
```
SimpleFilter has a **single-select-input** for selection filters by default. 

If you need to select multiple items you can use the `multiple: true` option. It generates a HTML ``<select multiple>`` tag.

With `multiple: :checkbox` you have checkboxes to select the items instead of the select-input.


#### Reference Models
Column filter on **reference tables** needs the `:reference` option.

So if you want to provide a text filter on the **:name** attribute of your **belongs_to :country**-reference, you can configure your filter like this:

```ruby
class User < ApplicationRecord
  belongs_to :country
  
  setup_filter_for :my_first_filter, :name, reference: :country 
end
```
You can also create a selection filter on references of your reference model.

Imagine your country model has a **belongs_to :continent**-reference and your continents table has also a **:name**-column.
 
Now you can filter on which continent a user lives.
```ruby
class Country < ApplicationRecord
  belongs_to :continent
end
  
class User < ApplicationRecord
  belongs_to :country
  
  setup_filter_for :my_first_filter, :continent, reference: :country, value_method: :name 
end
```

#### Labels
SimpleFilter uses the Model-Localization of your Rails app by default. 

So for columns `SomeModel.human_attribute_name(:some_column)` or `SomeModel.model_name.human` for filters on reference models.

But you can also set a custom label with the `:label` option.


#### Conditions
SimpleFilter has different sets of filter-conditions depending on the datatype of the filter. You can see the defaults **[here](lib/simple_filter/condition.rb)**.

You can create an initializer and override these conditions globally for your app.
```ruby
#/config/initializers/simple_filter.rb
require "simple_filter"

SimpleFilter::Condition.string_conditions = [:is, :is_not]
```
#### Multiple filters for one model
It is also possible to have more than one filter for a model.

All configurations with the same **filter_id** like `:my_first_filter` will be rendered to the same Filter-UI.

`setup_filter_for :my_special_user_filter, :some_column` will create a different filter configuration and can be rendered with `render_simple_filter(:my_special_user_filter,User)`.


#### Further configurations
You can extend the `render_simple_filter(filter_id,model,options)`-ViewHelper with a few options.


| Option        | Default           |Function   |
| ------------- |-------------|------|
| **panel_header_title:**      | Pluralized localization of the Filter-Model | You can override the default title or hide the header with  `panel_header_title: false`|
| **panel_collapsed:**      | false      |  The Filter-Panel should be collapsed on page load or not (**no collapsing if panel_header_title: false**)  |
| **panel_class:** | panel-default      |  Changes Bootstrap3 [Panel-Style](https://www.w3schools.com/bootstrap/bootstrap_panels.asp) |
| **filter_remote:** | false      |  Default SimpleFilter does a location reload after posting the filter state. If you enable **filter_remote**, SimpleFilter sends an Ajax-GET-Request (dataType: 'script') to the current location after posting the filter state. This GET-Request also sends the **filter_id** `params[:simple_filter][:filter_id]`. In case of having multiple lists/filters on one page, you can distinguish the requests from different filters in your controller action.|



## I18n
Included localizations are **en** and **de**. You can add or change existing localizations in your `config/locales/*.yml` directory.

**Important:** The [Bootstrap Datepicker](https://bootstrap-datepicker.readthedocs.io/en/latest/index.html) needs an own localization file for each language.
You have to download and include additional languages in your **/app/assets/javascripts/application.js**.


## Todos
  * Bug: Capture **(e.keyCode == 13)** in Filter-Form. Now it sends an GET-Request to current location.
  * Extend the filter for  **:has_and_belongs_to_many**-references
  * Responsive-UI optimization
  * Multiple **storable filters** per **filter_id** (session and/or database). Now only the current filter state of a **filter_id** is stored
  * Don't show second inputs for Range-Filters if range conditions are removed
  * Cleanup JS-Code. JS-Hooks like onFilterSuccess() etc.
  * The Datepicker uses always the 'dd-mm-yyyy'-format independend of current localization. This prevents Date.parse-Errors when switching the language, but is not the optimal solution




## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/a.n.d.y/simple-filter.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
