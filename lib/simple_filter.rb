require "simple_filter/version"
require "simple_filter/engine"
require "simple_filter/filter_storage"
require "simple_filter/model_extension"
require "simple_filter/controller_extension"
require "simple_filter/join_builder"
require "simple_filter/query_builder"
require "simple_filter/condition"
require "simple_filter/errors"


module SimpleFilter

  @@filter_configurations = {}

  def self.filter_configurations
    @@filter_configurations
  end

  def self.find_by_model(ar_model)
    ar_model = ar_model.to_s.underscore.to_sym
    filter_configurations[ar_model]
  end

  def self.find_by_model_and_filter_name(ar_model, filter_name)
    filter_name = filter_name.to_sym
    find_by_model(ar_model)[filter_name]
  end

end
