module SimpleFilter
  module FilterStorage

    def get_filter_state(filter_id, ar_model)
      if true #SimpleFilter::Config.filter_storage == :session
        ar_model = ar_model.to_s.underscore.to_sym
        session[:simple_filter] ||= {}
        session[:simple_filter].deep_symbolize_keys!
        session[:simple_filter][ar_model] ||= {}
        session[:simple_filter][ar_model][filter_id] ||= {}
        session[:simple_filter][ar_model][filter_id][:filter_state] ||= {}
        return session[:simple_filter][ar_model][filter_id][:filter_state]
      end
    end

    def set_filter_state(filter_id, ar_model, filter_state={})
      if true #SimpleFilter::Config.filter_storage == :session
        ar_model = ar_model.to_s.underscore.to_sym
        session[:simple_filter] ||= {}
        session[:simple_filter].deep_symbolize_keys!
        session[:simple_filter][ar_model] ||= {}
        session[:simple_filter][ar_model][filter_id] ||= {}
        session[:simple_filter][ar_model][filter_id][:filter_state] ||= {}

        session[:simple_filter][ar_model][filter_id][:filter_state] = filter_state
      end
    end


  end
end