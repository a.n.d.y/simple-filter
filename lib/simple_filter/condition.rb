module SimpleFilter
  module Condition

    INACTIVE_CONDITION = :not_active.freeze




    ALLOWED_DATE_CONDITIONS = [
        :is,
        :is_not,
        :between,
        :not_between,
        :bigger_than,
        :smaller_than
    ].freeze
    
    def self.date_conditions=(conditions)
      raise SimpleFilter::InvalidCondition, conditions-ALLOWED_DATE_CONDITIONS if (conditions-ALLOWED_DATE_CONDITIONS).present?   	    
      @@date_conditions = conditions
    end
    
    def self.date_conditions
      @@date_conditions ||= ALLOWED_DATE_CONDITIONS
    end
    singleton_class.send(:alias_method, :datetime_conditions, :date_conditions)




    ALLOWED_INTEGER_CONDITIONS = [
        :is,
        :is_not,
        :between,
        :not_between,
        :bigger_than,
        :smaller_than
    ].freeze
    
    def self.integer_conditions=(conditions)
      raise SimpleFilter::InvalidCondition, conditions-ALLOWED_INTEGER_CONDITIONS if (conditions-ALLOWED_INTEGER_CONDITIONS).present?   	    
      @@integer_conditions = conditions
    end
    
    def self.integer_conditions
      @@integer_conditions ||= ALLOWED_INTEGER_CONDITIONS
    end
    singleton_class.send(:alias_method, :decimal_conditions, :integer_conditions)




    ALLOWED_STRING_CONDITIONS = [
        :is,
        :is_not,
        :starts_with,
        :ends_with,
        :include,
        :include_not
    ].freeze

    def self.string_conditions=(conditions)
      raise SimpleFilter::InvalidCondition, conditions-ALLOWED_STRING_CONDITIONS if (conditions-ALLOWED_STRING_CONDITIONS).present?   	    
      @@string_conditions = conditions
    end
    
    def self.string_conditions
      @@string_conditions ||= ALLOWED_STRING_CONDITIONS
    end
    singleton_class.send(:alias_method, :text_conditions, :string_conditions)




    ALLOWED_BOOLEAN_CONDITIONS = [
        :is,
        :is_not
    ].freeze
    
    def self.boolean_conditions=(conditions)
      raise SimpleFilter::InvalidCondition, conditions-ALLOWED_BOOLEAN_CONDITIONS if (conditions-ALLOWED_BOOLEAN_CONDITIONS).present?   	    
      @@boolean_conditions = conditions
    end
    
    def self.boolean_conditions
      @@boolean_conditions ||= ALLOWED_BOOLEAN_CONDITIONS
    end




    ALLOWED_SELECTION_CONDITIONS = [
        :is,
        :is_not
    ].freeze
    
    def self.selection_conditions=(conditions)
      raise SimpleFilter::InvalidCondition, conditions-ALLOWED_SELECTION_CONDITIONS if (conditions-ALLOWED_SELECTION_CONDITIONS).present?   	    
      @@selection_conditions = conditions
    end
    
    def self.selection_conditions
      @@selection_conditions ||= ALLOWED_SELECTION_CONDITIONS
    end




    ALLOWED_BELONGS_TO_REFLECTION_CONDITIONS = [
        :is,
        :is_not
    ].freeze
    
    def self.belongs_to_reflection_conditions=(conditions)
      raise SimpleFilter::InvalidCondition, conditions-ALLOWED_BELONGS_TO_REFLECTION_CONDITIONS if (conditions-ALLOWED_BELONGS_TO_REFLECTION_CONDITIONS).present?   	    
      @@belongs_to_reflection_conditions = conditions
    end
    
    def self.belongs_to_reflection_conditions
      @@belongs_reflection_conditions ||= ALLOWED_BELONGS_TO_REFLECTION_CONDITIONS
    end
    
    
    
    
    ALLOWED_HAS_MANY_REFLECTION_CONDITIONS = [
        :is,
	      :is_not
    ].freeze
    
    def self.has_many_reflection_conditions=(conditions)
      raise SimpleFilter::InvalidCondition, conditions-ALLOWED_HAS_MANY_REFLECTION_CONDITIONS if (conditions-ALLOWED_HAS_MANY_REFLECTION_CONDITIONS).present?   	    
      @@has_many_reflection_conditions = conditions
    end
    
    def self.has_many_reflection_conditions
      @@has_many_reflection_conditions ||= ALLOWED_HAS_MANY_REFLECTION_CONDITIONS
    end
    
    
    
    
    ALLOWED_HAS_MANY_REFLECTION_MULTIPLE_CONDITIONS = [
        :is_all,
	      :is_any,
        :is_not_all,
	      :is_not_any
    ].freeze
    
    def self.has_many_reflection_multiple_conditions=(conditions)
      raise SimpleFilter::InvalidCondition, conditions-ALLOWED_HAS_MANY_REFLECTION_MULTIPLE_CONDITIONS if (conditions-ALLOWED_HAS_MANY_REFLECTION_MULTIPLE_CONDITIONS).present?
      @@has_many_reflection_multiple_conditions = conditions
    end
    
    def self.has_many_reflection_multiple_conditions
      @@has_many_reflection_multiple_conditions ||= ALLOWED_HAS_MANY_REFLECTION_MULTIPLE_CONDITIONS
    end




  end
end