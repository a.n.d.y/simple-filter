module SimpleFilter
  module QueryBuilder

    extend SimpleFilter::JoinBuilder

    class << self


      DEFAULT_STRING_SEPERATOR = ";".freeze
      DEFAULT_QUERY_CONNECTOR = "AND".freeze


      def build_filtered_relation(filter_id, ar_model, filter_state)
        filter_query = []
        filter_values = []
        filter_havings = []

        filter_id = filter_id.to_sym

        filter_config = SimpleFilter.find_by_model_and_filter_name(ar_model.to_s.underscore.to_sym,filter_id)
        ar_model = ar_model.to_s.classify.constantize


        if filter_state.present?

          joins, arel_tables = create_arel_table_joins(filter_id, ar_model, filter_state)

          filter_state.each do |filter|

            condition = filter[1][:condition]
            value_1 = filter[1][:value_1]
            value_2 = filter[1][:value_2]


            unless condition.to_sym == :not_active

              filter_item_key = filter[0]
              column_config = filter_config[filter_item_key]
              column_name = column_config[:column_name]

              target_model = ar_model
              left_arel_table = target_model.arel_table
              if column_config[:reference].present?
                reflection = target_model.reflections.symbolize_keys[column_config[:reference]]
                target_model = reflection.klass
                left_arel_table = arel_tables[arel_table_key(reflection)]
              end



              case column_config[:filter_type]
                when *[:string, :text]
                  query, values = string_query_builder(column_name,left_arel_table,condition,value_1)
                  filter_query << query
                  filter_values+=values
                when *[:integer, :decimal]
                  query, values = integer_query_builder(column_name,left_arel_table,condition,value_1, value_2)
                  filter_query << query
                  filter_values+=values
                when *[:date, :datetime]
                  query, values = date_query_builder(column_name,left_arel_table,condition,value_1, value_2)
                  filter_query << query
                  filter_values+=values
                when :boolean
                  query, values = boolean_query_builder(column_name,left_arel_table,condition,value_1)
                  filter_query << query
                  filter_values+=values
                when :selection
                  query, values = selection_query_builder(column_name,left_arel_table,condition,value_1)
                  filter_query << query
                  filter_values+=values
                when *[:belongs_to_reflection, :has_many_reflection]
                  query, values, havings = reflection_query_builder(column_config[:reflection_name],arel_tables, target_model, condition,value_1)
                  filter_query << query
                  filter_values+=values
                  filter_havings+=havings
                else
                  raise SimpleFilter::UnknownFilterType, column_config[:filter_type]
              end
	      
	          end
          end
        end

        conditions = []
        if(filter_query.present? || filter_values.present?)
          conditions[0] = filter_query.join(" #{DEFAULT_QUERY_CONNECTOR} ")
          filter_values.each do |value|
            conditions.push(value)
          end
        end

        model = ar_model.where(conditions)
        model = model.joins(joins.uniq) if joins.present?
        filter_havings.each{|having_condition| model = model.having(having_condition)}
        model = model.group(ar_model.primary_key.to_sym)
        model = model.distinct(model)

        return model
      end











      def integer_query_builder(column, arel_table, condition, value_1, value_2)
        query = ""
        values = []

        db_column = "#{arel_table.table_alias ? arel_table.table_alias : arel_table.table_name}.#{column}"

        if value_1.blank?
          #filter on blank value
          case condition.to_sym
            when :is
              query = db_column + " IS NULL"
            when :is_not
              query = db_column + " IS NOT NULL"
            when :between
              query = db_column + " IS NULL"
            when :not_between
              query = db_column + " IS NOT NULL"
            when :bigger_than
              query = db_column + " IS NOT NULL"
            when :smaller_than
              query = db_column + " IS NOT NULL"
            else
              raise SimpleFilter::InvalidCondition, condition
          end

        else

          value_1 = value_1.to_f
          value_2 = value_2.to_f
          case condition.to_sym
            when :is
              query = db_column + " = ?"
              values.push(value_1)
            when :is_not
              query = db_column + " != ?"
              values.push(value_1)
            when :between
              query = db_column + " BETWEEN ? AND ?"
              values.push(value_1)
              values.push(value_2)
            when :not_between
              query = db_column + " NOT BETWEEN ? AND ?"
              values.push(value_1)
              values.push(value_2)
            when :bigger_than
              query = db_column + " > ?"
              values.push(value_1)
            when :smaller_than
              query = db_column + " < ?"
              values.push(value_1)
            else
              raise SimpleFilter::InvalidCondition, condition
          end
        end

        return query, values
      end


      def string_query_builder(column, arel_table, condition, value_1)
        query = ""
        values = []

        db_column = "#{arel_table.table_alias ? arel_table.table_alias : arel_table.table_name}.#{column}"

        #user can filter with multiple string values
        text_values = value_1.to_s.split(DEFAULT_STRING_SEPERATOR).map(&:strip)


        if text_values.blank?
          #filter on blank value
          case condition.to_sym
            when :is
              query = db_column + " IS NULL"
            when :is_not
              query = db_column + " IS NOT NULL"
            when :starts_with
              query = db_column + " LIKE ?"
              values.push(" %")
            when :ends_with
              query = db_column + " LIKE ?"
              values.push("% ")
            when :include
              query = db_column + " LIKE ?"
              values.push("% %")
            when :include_not
              query = db_column + " NOT LIKE ?"
              values.push("% %")
            else
              raise SimpleFilter::InvalidCondition, condition
          end

        else

          case condition.to_sym
            when :is
              query = ("LOWER(#{db_column}) IN (?)")
              values.push(text_values.collect {|v| v.to_s.downcase})
            when :is_not
              query = ("LOWER(#{db_column}) NOT IN (?)")
              values.push(text_values.collect {|v| v.to_s.downcase})
            when :starts_with
              query = "("
              for val_index in 0...text_values.length do
                if(val_index == 0)
                  #first element
                  query += "LOWER(#{db_column}) LIKE ?"
                else
                  query += " OR LOWER(#{db_column}) LIKE ?"
                end
                values.push(text_values[val_index].to_s.downcase + "%")
              end
              query += ')'
            when :ends_with
              query = "("
              for val_index in 0...text_values.length do
                if(val_index == 0)
                  #first element
                  query += "LOWER(#{db_column}) LIKE ?"
                else
                  query += " OR LOWER(#{db_column}) LIKE ?"
                end
                values.push("%"+ text_values[val_index].to_s.downcase)
              end
              query += ')'
            when :include
              query = "("
              for val_index in 0...text_values.length do
                if(val_index == 0)
                  #first element
                  query += "LOWER(#{db_column}) LIKE ?"
                else
                  query += " OR LOWER(#{db_column}) LIKE ?"
                end
                values.push("%"+ text_values[val_index].to_s.downcase + "%")
              end
              query += ')'
            when :include_not
              query = "(("
              for val_index in 0...text_values.length do
                if(val_index == 0)
                  #first element
                  query += "LOWER(#{db_column}) NOT LIKE ?"
                else
                  #we need AND conector here
                  query += " AND LOWER(#{db_column}) NOT LIKE ?"
                end
                values.push("%"+ text_values[val_index].to_s.downcase + "%")
              end
              query += ') OR ' + db_column.to_s + ' IS NULL)' #field could also be NULL
            else
              raise SimpleFilter::InvalidCondition, condition
          end
        end

        return query, values
      end


      def date_query_builder(column, arel_table, condition, value_1, value_2)
        query = ""
        values = []

        db_column = "#{arel_table.table_alias ? arel_table.table_alias : arel_table.table_name}.#{column}"

        if value_1.blank?
          #filter on blank value
          case condition.to_sym
            when :is
              query = db_column + " IS NULL"
            when :is_not
              query = db_column + " IS NOT NULL"
            when :between
              query = db_column + " IS NULL"
            when :not_between
              query = db_column + " IS NOT NULL"
            when :bigger_than
              query = db_column + " IS NOT NULL"
            when :smaller_than
              query = db_column + " IS NOT NULL"
            else
              raise SimpleFilter::InvalidCondition, condition
          end

        else

          value_1 = value_1.to_date
          value_2 = value_2.to_date unless value_2.blank?
          case condition.to_sym
            when :is
              query = db_column + " BETWEEN ? AND ?"
              values.push(value_1.to_time)
              values.push(value_1.to_time + 1.day - 1.second)
            when :is_not
              query = db_column + " != ?"
              values.push(value_1)
            when :between
              query = db_column + " BETWEEN ? AND ?"
              values.push(value_1.to_time)
              values.push(value_2.to_s.blank? ? (value_1.to_time + 1.day - 1.second) : value_2.to_time)
            when :not_between
              query = db_column + " NOT BETWEEN ? AND ?"
              values.push(value_1.to_time)
              values.push(value_2.to_s.blank? ? (value_1.to_time + 1.day - 1.second) : value_2.to_time)
            when :bigger_than
              query = db_column + " > ?"
              values.push(value_1)
            when :smaller_than
              query = db_column + " < ?"
              values.push(value_1)
            else
              raise SimpleFilter::InvalidCondition, condition
          end
        end

        return query, values
      end


      def boolean_query_builder(column, arel_table, condition, value_1)
        query = ""
        values = []

        db_column = "#{arel_table.table_alias ? arel_table.table_alias : arel_table.table_name}.#{column}"

        if value_1.blank?
          #filter on blank value
          case condition.to_sym
            when :is
              query = db_column + " IS NULL"
            when :is_not
              query = db_column + " IS NOT NULL"
            else
              raise SimpleFilter::InvalidCondition, condition
          end

        else

          case condition.to_sym
            when :is
              query = db_column + " = ?"
              values.push(value_1.to_i == 1 ? true : false)
            when :is_not
              query = db_column + " != ?"
              values.push(value_1.to_i == 1 ? true : false)
            else
              raise SimpleFilter::InvalidCondition, condition
          end
        end

        return query, values
      end


      def selection_query_builder(column, arel_table, condition, value_1)
        query = ""
        values = []

        db_column = "#{arel_table.table_alias ? arel_table.table_alias : arel_table.table_name}.#{column}"

        if value_1.blank? || (value_1.class == Array && value_1.length == 1 && value_1.first.blank?) #also check empty arrays from multiselect select_tags [""]
          #filter on blank value
          case condition.to_sym
            when :is
              query = db_column + " IS NULL"
            when :is_not
              query = db_column + " IS NOT NULL"
            else
              raise SimpleFilter::InvalidCondition, condition
          end

        else

          case condition.to_sym
            when :is
              query = db_column + " IN (?)"
              values.push(value_1)
            when :is_not
              query = db_column + " NOT IN (?)"
              values.push(value_1)
            else
              raise SimpleFilter::InvalidCondition, condition
          end
        end

        return query, values
      end


      def reflection_query_builder(reflection_name, arel_tables, ar_model, condition, value_1)
        query = ""
        values = []
        havings = []

        reflection = ar_model.reflections.symbolize_keys[reflection_name]

        case reflection.macro

          when :belongs_to

            arel_table = arel_tables[arel_table_key(reflection)]
            db_column = "#{arel_table.table_alias}.#{reflection.association_primary_key}"

            if value_1.blank? || (value_1.class == Array && value_1.length == 1 && value_1.first.blank?) #also check empty arrays from multiselect select_tags [""]
              #filter on blank value
              case condition.to_sym
                when :is
                  query = db_column + " IS NULL"
                when :is_not
                  query = db_column + " IS NOT NULL"
                else
                  raise SimpleFilter::InvalidCondition, condition
              end

            else

              case condition.to_sym
                when :is
                  query = db_column + " IN (?)"
                  values.push(value_1)
                when :is_not
                  query = "(#{db_column} NOT IN (?)"
                  query += " OR #{db_column} IS NULL)"
                  values.push(value_1)
                else
                  raise SimpleFilter::InvalidCondition, condition
              end
            end

          when :has_many

            arel_table = arel_tables[arel_table_key(reflection)]
            db_column = "#{arel_table.table_alias}.#{reflection.association_primary_key}"

            if value_1.blank? || (value_1.class == Array && value_1.length == 1 && value_1.first.blank?) #also check empty arrays from multiselect select_tags [""]
              #filter on blank value
              case condition.to_sym
                when :is
                  query = "#{db_column} IS NULL"
                when :is_not
                  query = "#{db_column} IS NOT NULL"
                when :is_any
                  query = "#{db_column} IS NULL"
                when :is_not_any
                  query = "#{db_column} IS NOT NULL"
                when :is_all
                  query = "#{db_column} IS NULL"
                when :is_not_all
                  query = "#{db_column} IS NOT NULL"
                else
                  raise SimpleFilter::InvalidCondition, condition
              end

            else

              case condition.to_sym
                when :is
                  query = "#{db_column} IN (?)"
                  values.push(value_1)
                when :is_not
                  query = "(#{db_column} NOT IN (?)"
                  query += " OR #{db_column} IS NULL)"
                  values.push(value_1)
                when :is_any
                  query = "#{db_column} IN (?)"
                  values.push(value_1)
                when :is_not_any
                  #### fix this #####
                  query = "(#{db_column} NOT IN (?)"
                  query += " OR #{db_column} IS NULL)"
                  values.push(value_1)
                when :is_all
                  query = "#{db_column} IN (?)"
                  values.push(value_1)
                  havings.push("COUNT(#{db_column}) = #{value_1.size}")
                when :is_not_all
                  query = "(#{db_column} NOT IN (?)"
                  query += " OR #{db_column} IS NULL)"
                  values.push(value_1)
                else
                  raise SimpleFilter::InvalidCondition, condition
              end
            end

          else
            raise SimpleFilter::UnhandledReflectionType, reflection.macro
        end

        return query, values, havings
      end

    end
  end
end