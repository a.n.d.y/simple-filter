module SimpleFilter
  class Error < StandardError; end

  class InvalidCondition < Error; end
  class UnknownFilterType < Error; end
  class UnhandledReflectionType < Error; end

end