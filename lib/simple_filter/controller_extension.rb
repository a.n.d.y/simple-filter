module SimpleFilter
  module ControllerExtension
    
    include SimpleFilter::FilterStorage
    
    def build_filtered_relation(filter_id, ar_model)
      filter_id = filter_id.to_sym
      ar_model = ar_model.to_s.underscore.to_sym
      filter_state = get_filter_state(filter_id, ar_model)

      SimpleFilter::QueryBuilder.build_filtered_relation(filter_id, ar_model, filter_state)
    end
  
  end
end