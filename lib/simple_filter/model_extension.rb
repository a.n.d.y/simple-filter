module SimpleFilter
  module ModelExtension
    extend ActiveSupport::Concern

    REQUIRED_REFLECTION_OPTIONS = [
        :value_method
    ].freeze

    REQUIRED_SELECTION_OPTIONS = [
    ].freeze


    included do

    end



    class_methods do

      def setup_filter_for(filter_id, column_or_reference, options={})

        filter_id = filter_id.to_s.downcase.to_sym
        column_or_reference = column_or_reference.to_sym

        filter_model = self
        target_model = filter_model


        if options[:reference].present?
          reference_object = filter_model.reflections.symbolize_keys[options[:reference]]
          raise SimpleFilter::Error, "Unknown reference '#{options[:reference]}' for model '#{filter_model}'!" if reference_object.blank?
          target_model = reference_object.klass
        end

        setup_configuration(filter_id, column_or_reference, filter_model, target_model, options)

      end



      def setup_configuration(filter_id, column_or_reference, filter_model, target_model, options={})

        column = target_model.columns_hash.symbolize_keys[column_or_reference]
        reflection = target_model.reflections.symbolize_keys[column_or_reference]

        raise SimpleFilter::Error, "Unknown column or reference '#{column_or_reference}' for class '#{target_model}' in '#{filter_model}.setup_filter_for' !" if column.blank? && reflection.blank?

        if reflection
          unless REQUIRED_REFLECTION_OPTIONS.all? {|option| options.keys.include? option}
            raise SimpleFilter::Error, "Missing options '#{(REQUIRED_REFLECTION_OPTIONS-options.keys).inspect}' for reference '#{column_or_reference}'!"
          end
          options.merge!({filter_type: "#{reflection.macro}_reflection".to_sym})
          options.merge!({reflection_name: reflection.name.to_sym})
        elsif column
          if options.keys.include?(:select_options)
            unless REQUIRED_SELECTION_OPTIONS.all? {|option| options.keys.include? option}
              raise SimpleFilter::Error, "Missing options '#{(REQUIRED_SELECTION_OPTIONS-options.keys).inspect}' for selection on column '#{column_or_reference}'!"
            end
            options.merge!({filter_type: :selection})
          else
            options.merge!({filter_type: column.type.to_sym})
          end
          options.merge!({column_name: column.name.to_sym})
        end


        filter_model_key = filter_model.to_s.downcase.to_sym
        filter_item_key = "#{options[:reference] ? options[:reference].to_s+'_' : ''}#{target_model.name.downcase}_#{column ? column.name : reflection.name}".to_sym

        if SimpleFilter.filter_configurations.keys.include? filter_model_key
          filter_model_filters = SimpleFilter.filter_configurations[filter_model_key]
          if filter_model_filters.keys.include? filter_id
            SimpleFilter.filter_configurations[filter_model_key][filter_id].merge!(filter_item_key => options)
          else
            SimpleFilter.filter_configurations[filter_model_key][filter_id] = {filter_item_key => options}
          end
        else
          SimpleFilter.filter_configurations[filter_model_key] = {filter_id => {filter_item_key => options}}
        end

      end


    end

  end
end