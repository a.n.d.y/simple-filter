  module SimpleFilter
    class Engine < Rails::Engine
      isolate_namespace SimpleFilter
      
      initializer "simple_filter.view_helpers" do
        ActionView::Base.send :include, SimpleFilter::Helper
      end

      initializer "simple_filter.controller" do |app|
        ActionController::Base.send :include, SimpleFilter::ControllerExtension
      end

      initializer "simple_filter.model" do |app|
        ActiveRecord::Base.send :include, SimpleFilter::ModelExtension
      end
    
      config.i18n.load_path += Dir["#{config.root}/config/locales/simple_filter/*.yml"]
    
    end
  end