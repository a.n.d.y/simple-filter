module SimpleFilter
  module JoinBuilder

    def arel_table_key(reflection)
      "#{reflection.active_record.name.tableize}_#{reflection.plural_name}".to_sym
    end

    def create_arel_table_join(left_arel_table, reflection, arel_tables, joins)
      if reflection.macro == :belongs_to
        unless arel_tables.keys.include? arel_table_key(reflection)
          right_model = reflection.klass
          arel_tables[arel_table_key(reflection)] = right_arel_table = Arel::Table.new(right_model.table_name, as: "#{arel_table_key(reflection)}")
          joins << left_arel_table.outer_join(right_arel_table).on(left_arel_table[reflection.association_foreign_key].eq(right_arel_table[reflection.association_primary_key])).join_sources
        else
          right_arel_table = arel_tables[arel_table_key(reflection)]
        end
      elsif reflection.macro == :has_many
        if reflection.options[:through]
          left_model = reflection.active_record
          middle_reflection = left_model.reflections.symbolize_keys[reflection.options[:through]]
          right_reflection = middle_reflection.klass.reflections.symbolize_keys[reflection.options[:source]]
          middle_model = middle_reflection.klass
          right_model = right_reflection.klass

          unless arel_tables.keys.include? arel_table_key(middle_reflection)
            arel_tables[arel_table_key(middle_reflection)] = middle_arel_table = Arel::Table.new(middle_model.table_name, as: "#{arel_table_key(reflection)}")
            joins << left_arel_table.outer_join(middle_arel_table).on(left_arel_table[middle_reflection.association_primary_key].eq(middle_arel_table[middle_reflection.foreign_key])).join_sources
          else
            middle_arel_table = arel_tables[arel_table_key(middle_reflection)]
          end
          unless arel_tables.keys.include? arel_table_key(reflection)
            arel_tables[arel_table_key(reflection)] = right_arel_table = Arel::Table.new(right_model.table_name, as: "#{arel_table_key(reflection)}")
            joins << middle_arel_table.outer_join(right_arel_table).on(middle_arel_table[right_reflection.association_foreign_key].eq(right_arel_table[right_reflection.association_primary_key])).join_sources
          else
            right_arel_table = arel_tables[arel_table_key(reflection)]
          end

        else
          unless arel_tables.keys.include? arel_table_key(reflection)
            right_model = reflection.klass
            arel_tables[arel_table_key(reflection)] = right_arel_table = Arel::Table.new(right_model.table_name, as: "#{arel_table_key(reflection)}")
            joins << left_arel_table.outer_join(right_arel_table).on(left_arel_table[reflection.association_primary_key].eq(right_arel_table[reflection.foreign_key])).join_sources
          else
            right_arel_table = arel_tables[arel_table_key(reflection)]
          end
        end
      end
      return joins, arel_tables
    end

    def create_arel_table_joins(filter_id, ar_model, filter_state)
      filter_id = filter_id.to_sym
      filter_config = SimpleFilter.find_by_model_and_filter_name(ar_model.to_s.underscore.to_sym,filter_id)
      ar_model = ar_model.to_s.classify.constantize

      arel_tables = {}
      joins = []

      filter_state.each do |filter|
        filter_item_key = filter[0]
        column_config = filter_config[filter_item_key]

        left_model = ar_model
        left_arel_table = left_model.arel_table

        reflection_key = column_config[:reflection_name]
        reference_key = column_config[:reference]

        if reference_key && reflection_key
          reference = left_model.reflections.symbolize_keys[reference_key]
          reflection = reference.klass.reflections.symbolize_keys[reflection_key]
          joins, arel_tables = create_arel_table_join(left_arel_table,reference,arel_tables, joins)
          joins, arel_tables = create_arel_table_join(arel_tables[arel_table_key(reference)],reflection,arel_tables, joins)
        elsif reference_key
          reference = left_model.reflections.symbolize_keys[reference_key]
          joins, arel_tables = create_arel_table_join(left_arel_table,reference,arel_tables, joins)
        elsif reflection_key
          reflection = left_model.reflections.symbolize_keys[reflection_key]
          joins, arel_tables = create_arel_table_join(left_arel_table,reflection,arel_tables, joins)
        end

      end

      return joins, arel_tables
    end


  end
end

